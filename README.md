# MetaSat Schema

The MetaSat team is working to create a metadata vocabulary, example schemas, and crosswalks to describe satellite missions. MetaSat is specifically optimized for small satellite (SmallSat) missions. This work will help facilitate information sharing between missions and lower the barrier of entry into the field.  

## Why metadata?

Metadata is most simply defined as data about data. For example, a book contains data: the words on the page. The metadata is information about the book, such as the title, author, page count, publisher, and more. Space missions also have data&mdash;for example, the information a satellite collects and sends back to earth. The metadata about a space mission might include information about the satellite's components, people and organizations involved with the mission, the launch date, and information about ground stations that communicate with the spacecraft.  

Right now, space mission metadata is recorded in many different ways, which makes it hard for different teams to talk to each other, collaborate, or share advice about what works or doesn't work in SmallSat missions. Our goal with MetaSat is to create a tool kit that can be shared and expanded upon to respond to and anticipate this community's needs.  

## Using MetaSat

The MetaSat vocabulary is the core of the MetaSat project. The MetaSat vocabulary is a list of unique terms that can be used to describe spacecraft, missions, ground stations, and more, each with a unique, permanent URI. A URI, or **U**niform **R**esource **I**dentifier, acts as a machine-readable identifier for the concept. The MetaSat vocabulary and its URIs can be used can be used to describe missions both in private databases and on the web; since each terms uses its own URI, the vocabulary can be used for linked data applications and schemas that use any format of the RDF data model, including RDF-XML, Turtle, N-Triples, and more. *Unfamiliar with RDF or linked data? Visit [the W3C page on linked data](https://www.w3.org/standards/semanticweb/data) or [5-star Open Data](https://5stardata.info/en/).*  

We are also creating example schemas that can be used with our vocabulary. Our schemas use JSON-LD, a highly flexible form of RDF that is built to be easily human-writable and machine-readable. The schemas combine our vocabulary with structure, and give examples and recommendations for how terms relate to each other. For more information about how to use MetaSat, JSON-LD, and RDF, check out our write-ups on [getting started](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/getting_started.md) and [RDF](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/RDF.md). If you are already familiar with linked data but not JSON-LD, you can read our [JSON-LD primer here](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/json-ld_primer.md).

## Our work so far

The [Wolbach Library](https://library.cfa.harvard.edu/) at the Center for Astrophysics is working closely with the [Libre Space Foundation (LSF)](https://libre.space/) to create MetaSat. LSF has direct experience working on SmallSat missions and runs SatNOGS, a global network of ground stations that collects satellite observations and stores them in a database. The [SatNOGS Network](https://network.satnogs.org/) stores information about both the ground stations in the network and their observations, and the SatNOGS database ([SatNOGS DB](https://db.satnogs.org/)) stores information about active satellites. SatNOGS will be one of the earliest adopters of MetaSat.  

Would you like to contribute to the future of MetaSat? Submit an issue in this repository, email us at metasat[at]schema.space, or [visit our website](https://schema.space/) for more resources.  

## About this repository

This repository includes [JSON-LD](https://json-ld.org/) versions of the MetaSat Schema broken down by use case. If you have suggestions or comments please submit them as Issues on this repository.

Right now, the in-progress JSON-LD implementation of the MetaSat vocabulary has three major parts, which you will find in this repository. Our JSON-LD files contain all of the terms in a suggested hierarchy; please use them as a suggestion, not a mandate.

[metasat.jsonld](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/metasat.jsonld) contains all MetaSat terms.

The [examples folder](https://gitlab.com/metasat/metasat-toolkit/-/tree/master/examples) contains real examples encoded into JSON-LD. Most of our examples right now use SatNOGS-specific use cases.


#### Context file

We've included an [example of the `@context` section](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/context.jsonld) that helps give JSON-LD its structure and can turn your data into linked data. Our context file is based on the [JSON-LD recommended context](https://github.com/w3c/json-ld-rc/blob/master/context.jsonld), with a few additions. Please keep in mind that this is simply a jumping-off point, and is meant to inspire, not constrain. In addition, each of the example files in our [examples folder](https://gitlab.com/metasat/metasat-toolkit/-/tree/master/examples) links to our context file. For more information about how to import an externally hosted context file, check out our [JSON-LD primer](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/json-ld_primer.md).  

When using this example context, and in our example files, most concepts will map to "https://schema.space/metasat/concept." The rest are Compact URIs, or [CURIEs](https://www.w3.org/TR/curie/). A CURIE uses the aliases in the context to help make keys shorter. For example, our context file includes the line `"schema": "https://schema.org/"`. If you wanted a key to map to https://schema.org/Person, you could use the CURIE "schema:Person." The alias comes first, then a colon, and then whatever comes after the delimeter in the URI (in this case, the forward slash).  
