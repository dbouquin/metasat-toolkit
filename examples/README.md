# MetaSat Example Files

The files in this folder are complete MetaSat JSON-LD files that describe real space missions, satellites, ground stations, and more. These files combine a number of linked-data concepts, including combining vocabularies, using JSON-LD `@id`s, and linking concepts within a JSON-LD file.  

These files are only examples, and do not reflect the only way that MetaSat and JSON-LD could be used to describe their concepts.

- [lstn_kit_example.jsonld](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/examples/lstn_kit_example.jsonld) is an example JSON-LD file that uses MetaSat concepts to describe the basic components of a [LSTN kit](https://lstn.wolba.ch/parts/) as implemented by a SatNOGS ground station.


- [picard_mission_example.jsonld](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/examples/picard_mission_example.jsonld) is an example JSON-LD file that uses MetaSat concepts to generally describe a space mission. The space mission used in this example file is the CNES solar-terrestrial microsatellite mission [Picard](https://en.wikipedia.org/wiki/Picard_(satellite)).


- [qubik_mission_example.jsonld](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/examples/qubik_mission_example.jsonld) is an example JSON-LD file that uses MetaSat concepts to describe two PocketQube satellites ([QUBIK-1 & QUBIK-2](https://libre.space/projects/qubik/)) operated by the [Libre Space Foundation](https://libre.space/) (LSF).


- [satnogs_ground_station_example.jsonld](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/examples/satnogs_ground_station_example.jsonld) is an example JSON-LD file that uses MetaSat concepts to describe a basic SatNOGS ground station in the [SatNOGS DB](https://db.satnogs.org/).


- [satnogs_observation_example.jsonld](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/examples/satnogs_observation_example.jsonld) is an example JSON-LD file that uses MetaSat concepts to describe an [observation](https://network.satnogs.org/observations/) of a satellite using the SatNOGS ground station network.


- [sun_sensor_example.jsonld](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/examples/sun_sensor_example.jsonld) is an example JSON-LD file that uses MetaSat concepts to describe a typical sun sensor as found in NASA's [SmallSat Parts On Orbit Now (SPOON)](https://spoonsite.com/) database.
 
