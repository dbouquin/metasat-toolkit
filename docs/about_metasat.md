# Introducing MetaSat  

The MetaSat team is working to create both a metadata vocabulary to describe satellite missions and a set of tools to make the vocabulary easy to implement. The MetaSat vocabulary is specifically optimized for small satellite (SmallSat) missions. Our work will help facilitate information sharing between space missions and lower the barrier of entry into the field.  

## Why metadata?  

Metadata is most simply defined as data about data. For example, a book is filled with data: the words on the page. Its metadata is information about the book, such as the title, author, page count, publisher, and more. Space missions also produce data&mdash;for example, the information a satellite collects and sends back to earth. On the other hand, the mission's metadata might include information about the satellite's components, people and organizations involved with the mission, the launch date, and information about ground stations that communicate with the spacecraft.  

Right now, space mission metadata is recorded in many different ways, which makes it hard for different teams to talk to each other, collaborate, or share advice about what works or doesn't work in SmallSat missions. Our goal with MetaSat is to create a tool kit that can be shared and expanded upon to respond to and anticipate the satellite community's needs.  

## Using MetaSat  

The MetaSat vocabulary is the core of the MetaSat project. The MetaSat vocabulary is a list of unique concepts that can be used to describe spacecraft, missions, ground stations, and more, each with a unique, permanent URI. A URI, or Uniform Resource Identifier, acts as a machine-readable identifier for the concept. The MetaSat vocabulary and its URIs can be used to describe missions both in private databases and on the web; since each concept uses its own URI, the vocabulary can be used for linked data applications and schemas that use any format of the [RDF data model](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/RDF.md), including RDF-XML, Turtle, N-Triples, and more.  

We recommend using the MetaSat vocabulary with [JSON-LD](https://json-ld.org/), a highly flexible form of RDF that is built to be easily human-writable and machine-readable. To illustrate how JSON-LD can be used with MetaSat, our GitLab repository has multiple [example files](https://gitlab.com/metasat/metasat-toolkit/-/tree/master/examples) that describe real missions. They include descriptions of satellites and missions, as well as ground stations and observations. These files can be used to inspire your own use of JSON-LD with MetaSat. For more information about how to use MetaSat, JSON-LD, and RDF, check out our [metadata guide](https://gitlab.com/metasat/metasat-toolkit/-/blob/master/docs/getting_started.md).  

The final piece of the MetaSat toolkit involves an extensive crosswalk to many other metadata standards, dictionaries, and more. The crosswalks can be used to give each concept richer meaning, to better incorporate other linked data vocabularies, and to crosswalk between other standards and existing databases.  

## Our work so far  

The Wolbach library is working closely with the [Libre Space Foundation](https://libre.space/) (LSF) to create MetaSat. LSF has direct experience working on SmallSat missions and runs SatNOGS, a global network of ground stations that collects satellite observations and stores them in a database. The [SatNOGS Network](https://network.satnogs.org/) stores information about both the ground stations in the network and their observations, and the SatNOGS database ([SatNOGS DB](https://db.satnogs.org/)) stores information about active satellites. SatNOGS is one of the earliest adopters of MetaSat and has already incorporated MetaSat concepts into some of its APIs.

Interested in learning about what we're doing next, or contributing to the future of MetaSat? Visit [our GitLab repository](https://gitlab.com/metasat/metasat-toolkit) and submit an issue, or email us at metasat[at]schema.space.
